namespace TaskManagerLibrary.Models;

public class MyTask
{
    public int Id { get; set; }
    public string Title { get; set; }
    public string Description { get; set; }
    public Priority PriorityTask {get; set;}
    public DateTime CreationDate { get; set; }
    public DateTime? DueDate { get; set; }
    public bool IsCompleted { get; set; }
    public int? UserId { get; set; }
    public int? CategoryId { get; set; }
}

public enum Priority
{
    High,
    Medium,
    Low,
    Minima,
    No_Priority
}
