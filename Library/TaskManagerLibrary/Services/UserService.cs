using System.Text;

using Newtonsoft.Json;
using TaskManagerLibrary.Models;

namespace TaskManagerLibrary.Services;

public class UserService
    {
        private readonly HttpClient _client;

        public UserService(HttpClient client)
        {
            _client = client ?? throw new ArgumentNullException(nameof(client));
        }

        public async Task<IEnumerable<User>> GetAllUsers()
        {
            try
            {
                var response = await _client.GetAsync("api/User");
                response.EnsureSuccessStatusCode();

                var responseString = await response.Content.ReadAsStringAsync();

                using var responseStream = await response.Content.ReadAsStreamAsync();
                return JsonConvert.DeserializeObject<IEnumerable<User>>(responseString);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error getting users: {ex.Message}");
                return null;
            }
        }

        public async Task CreateUser(User user)
        {
            try
            {
                var json = JsonConvert.SerializeObject(user);

                var content = new StringContent(json, Encoding.UTF8, "application/json");

                var response = await _client.PostAsync("api/User", content);
                response.EnsureSuccessStatusCode();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error creating user: {ex.Message}");
            }
        }

        public async Task UpdateUser(int userId, User user)
        {
            try
            {
                var json = JsonConvert.SerializeObject(user);

                var content = new StringContent(json, Encoding.UTF8, "application/json");

                var response = await _client.PutAsync($"api/User/{userId}", content);
                response.EnsureSuccessStatusCode();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error updating user: {ex.Message}");
            }
        }

        public async Task DeleteUser(int userId)
        {
            try
            {
                var response = await _client.DeleteAsync($"api/User/{userId}");
                response.EnsureSuccessStatusCode();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error deleting user: {ex.Message}");
            }
        } 
    }
