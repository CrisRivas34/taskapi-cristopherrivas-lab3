using System.Text;

using Newtonsoft.Json;
using TaskManagerLibrary.Models;

namespace TaskManagerLibrary.Services;

public class CategoryService
    {
        private readonly HttpClient _client;

        public CategoryService(HttpClient client)
        {
            _client = client ?? throw new ArgumentNullException(nameof(client));
        }

        public async Task<IEnumerable<Category>> GetAllCategories()
        {
            try
            {
                var response = await _client.GetAsync("api/Category");
                response.EnsureSuccessStatusCode();

                var responseString = await response.Content.ReadAsStringAsync();

                using var responseStream = await response.Content.ReadAsStreamAsync();
                return JsonConvert.DeserializeObject<IEnumerable<Category>>(responseString);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error getting Categories: {ex.Message}");
                return null;
            }
        }

        public async Task CreateCategory(Category category)
        {
            try
            {
                var json = JsonConvert.SerializeObject(category);

                var content = new StringContent(json, Encoding.UTF8, "application/json");

                var response = await _client.PostAsync("api/Category", content);
                response.EnsureSuccessStatusCode();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error creating Category: {ex.Message}");
            }
        }

        public async Task UpdateCategory(int categoryId, Category category)
        {
            try
            {
                var json = JsonConvert.SerializeObject(category);

                var content = new StringContent(json, Encoding.UTF8, "application/json");

                var response = await _client.PutAsync($"api/Category/{categoryId}", content);
                response.EnsureSuccessStatusCode();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error updating Category: {ex.Message}");
            }
        }

        public async Task DeleteCategory(int categoryId)
        {
            try
            {
                var response = await _client.DeleteAsync($"api/Category/{categoryId}");
                response.EnsureSuccessStatusCode();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error deleting Category: {ex.Message}");
            }
        } 
    }
