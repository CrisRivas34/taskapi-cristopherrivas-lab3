using System.Text;

using Newtonsoft.Json;
using TaskManagerLibrary.Models;

namespace TaskManagerLibrary.Services;

public class MyTaskService
    {
        private readonly HttpClient _client;

        public MyTaskService(HttpClient client)
        {
            _client = client ?? throw new ArgumentNullException(nameof(client));
        }

        public async Task<IEnumerable<MyTask>> GetAllTasks()
        {
            try
            {
                var response = await _client.GetAsync("api/Task");
                response.EnsureSuccessStatusCode();

                var responseString = await response.Content.ReadAsStringAsync();

                using var responseStream = await response.Content.ReadAsStreamAsync();
                return JsonConvert.DeserializeObject<IEnumerable<MyTask>>(responseString);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error getting Tasks: {ex.Message}");
                return null;
            }
        }

        public async Task CreateTask(MyTask myTask)
        {
            try
            {
                var json = JsonConvert.SerializeObject(myTask);

                var content = new StringContent(json, Encoding.UTF8, "application/json");

                var response = await _client.PostAsync("api/Task", content);
                response.EnsureSuccessStatusCode();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error creating task: {ex.Message}");
            }
        }

        public async Task UpdateTask(int myTaskId, MyTask myTask)
        {
            try
            {
                var json = JsonConvert.SerializeObject(myTask);

                var content = new StringContent(json, Encoding.UTF8, "application/json");

                var response = await _client.PutAsync($"api/Task/{myTaskId}", content);
                response.EnsureSuccessStatusCode();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error updating task: {ex.Message}");
            }
        }

        public async Task DeleteTask(int myTaskId)
        {
            try
            {
                var response = await _client.DeleteAsync($"api/Task/{myTaskId}");
                response.EnsureSuccessStatusCode();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error deleting task: {ex.Message}");
            }
        } 
    }
