using System.Text;

using Newtonsoft.Json;
using TaskManagerLibrary.Models;

namespace TaskManagerLibrary.Services;

public class TaskStateHistoryService
    {
        private readonly HttpClient _client;

        public TaskStateHistoryService(HttpClient client)
        {
            _client = client ?? throw new ArgumentNullException(nameof(client));
        }

        public async Task<IEnumerable<TaskStateHistory>> GetTaskState(int taskStateId)
        {
            try
            {
                var response = await _client.DeleteAsync($"api/Task/{taskStateId}/state-history");
                response.EnsureSuccessStatusCode();

                var responseString = await response.Content.ReadAsStringAsync();

                using var responseStream = await response.Content.ReadAsStreamAsync();
                return JsonConvert.DeserializeObject<IEnumerable<TaskStateHistory>>(responseString);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error getting task state: {ex.Message}");
                return null;
            }
        }

        public async Task CreateTaskState(TaskStateHistory taskState, int taskStateId)
        {
            try
            {
                var json = JsonConvert.SerializeObject(taskState);

                var content = new StringContent(json, Encoding.UTF8, "application/json");

                var response = await _client.PostAsync($"api/Task/{taskStateId}/state-history", content);
                response.EnsureSuccessStatusCode();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error creating task state: {ex.Message}");
            }
        }

        public async Task UpdateTaskState(int taskStateId, TaskStateHistory taskState)
        {
            try
            {
                var json = JsonConvert.SerializeObject(taskState);

                var content = new StringContent(json, Encoding.UTF8, "application/json");

                var response = await _client.PutAsync($"api/Task/{taskStateId}/state-history", content);
                response.EnsureSuccessStatusCode();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error updating task state: {ex.Message}");
            }
        }

        public async Task DeleteUser(int taskStateId)
        {
            try
            {
                var response = await _client.DeleteAsync($"api/Task/{taskStateId}/state-history");
                response.EnsureSuccessStatusCode();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error deleting task state: {ex.Message}");
            }
        } 
    }
