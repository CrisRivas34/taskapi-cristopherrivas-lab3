using taskapi_cristopherrivas_lab3.Models;

namespace taskapi_cristopherrivas_lab3;

public class Category
{
    public int CategoryId {get; set;}
    public string Name {get; set;}
    public string Description {get; set;}
    public virtual ICollection<MyTask> Tasks {get; set;}
}