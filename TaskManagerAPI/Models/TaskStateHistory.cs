namespace taskapi_cristopherrivas_lab3;

public class TaskStateHistory
{
    public int Id { get; set; }
    public int TaskId { get; set; }
    public StateTask State { get; set; }
    public DateTime Timestamp { get; set; }
}

public enum StateTask
{
    NotStarted,
    InProgress,
    Closed
}
