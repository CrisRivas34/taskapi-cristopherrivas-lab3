using taskapi_cristopherrivas_lab3.Models;

namespace taskapi_cristopherrivas_lab3;

public class User
{
    public int Id { get; set; }
    public string Name { get; set; }

    public virtual ICollection<MyTask> Tasks { get; set; }
}
