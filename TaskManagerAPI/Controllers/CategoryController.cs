using Microsoft.AspNetCore.Mvc;
using taskapi_cristopherrivas_lab3.Models;
using taskapi_cristopherrivas_lab3.Services;

namespace taskapi_cristopherrivas_lab3.Controllers;

[Route("api/[controller]")]
public class CategoryController: ControllerBase
{
    ICategoryService categoryService;

    public CategoryController(ICategoryService service) {
        categoryService = service;
    }

    [HttpGet]
    public IActionResult Get()
    {
        return Ok(categoryService.Get());
    }

    [HttpPost]
    public IActionResult Post([FromBody] Category category)
    {
        categoryService.Save(category);
        return Created();
    }

    [HttpPut("{id}")]
    public IActionResult Put(int id, [FromBody] Category category)
    {
        categoryService.Update(id, category);
        return NoContent();
    }

    [HttpDelete("{id}")]
    public IActionResult Delete(int id)
    {
        categoryService.Delete(id);
        return NoContent();
    }
}