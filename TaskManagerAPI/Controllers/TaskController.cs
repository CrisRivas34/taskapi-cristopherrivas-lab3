using Microsoft.AspNetCore.Mvc;
using taskapi_cristopherrivas_lab3.Services;

namespace taskapi_cristopherrivas_lab3.Controllers;

[ApiController]
[Route("api/[controller]")]
public class TaskController : ControllerBase
{
    ITaskService taskService;

    public TaskController(ITaskService service)
    {
        taskService = service;
    }

    [HttpGet]
    public IActionResult Get()
    {
        return Ok(taskService.Get());
    }

    [HttpPost]
    public IActionResult Post([FromBody] MyTask myTask)
    {
        taskService.Save(myTask);
        return Created();
    }

    [HttpPut("{id}")]
    public IActionResult Put(int id, [FromBody] MyTask myTask)
    {
        taskService.Update(id, myTask);
        return NoContent();
    }

    [HttpDelete("{id}")]
    public IActionResult Delete(int id)
    {
        taskService.Delete(id);
        return NoContent();
    }

    [HttpGet("{taskId}/state-history")]
    public IActionResult GetTaskStateHistory(int taskId)
    {
        var history = taskService.GetTaskStateHistory(taskId);
        return Ok(history);
    }

    [HttpPost("{taskId}/state-history")]
    public IActionResult AddTaskStateHistory(int taskId, [FromBody] TaskStateHistory history)
    {
        taskService.AddTaskStateHistory(taskId, history);
        return Created();
    }

    [HttpPut("{taskId}/state-history")]
    public IActionResult PutState(int taskId, [FromBody] TaskStateHistory history)
    {
        taskService.UpdateState(taskId, history);
        return NoContent();
    }

    [HttpDelete("{taskId}/state-history")]
    public IActionResult DeleteState(int id)
    {
        taskService.DeleteState(id);
        return NoContent();
    }
}
