using Microsoft.AspNetCore.Mvc;
using taskapi_cristopherrivas_lab3.Services;

namespace taskapi_cristopherrivas_lab3.Controllers;

[ApiController]
[Route("api/[controller]")]
public class UserController : ControllerBase
{
    IUserService userService;

    public UserController(IUserService service)
    {
        userService = service;
    }

    [HttpGet]
    public IActionResult Get()
    {
        return Ok(userService.Get());
    }

    [HttpPost]
    public IActionResult Post([FromBody] User user)
    {
        userService.Save(user);
        return Created();
    }

    [HttpPut("{id}")]
    public IActionResult Put(int id, [FromBody] User user)
    {
        userService.Update(id, user);
        return NoContent();
    }

    [HttpDelete("{id}")]
    public IActionResult Delete(int id)
    {
        userService.Delete(id);
        return NoContent();
    }

}
