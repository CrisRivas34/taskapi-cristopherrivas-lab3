using FluentValidation.AspNetCore;
using Microsoft.EntityFrameworkCore;
using Pomelo.EntityFrameworkCore.MySql.Infrastructure;
using Microsoft.AspNetCore.Mvc;
using taskapi_cristopherrivas_lab3.Controllers;
using taskapi_cristopherrivas_lab3.Models;
using taskapi_cristopherrivas_lab3.Services;
using Pomelo.EntityFrameworkCore.MySql.Internal;
using taskapi_cristopherrivas_lab3.DataAccessLayer;


var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers()
    .AddFluentValidation(fv => fv.RegisterValidatorsFromAssemblyContaining<TaskValidator>());
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddDbContext<TaskContext>(options =>
    options.UseSqlite("Data Source=./DatabaseBackup/cristopher.rivas.sqlite"));
builder.Services.AddScoped<ICategoryService, CategoryService>();
builder.Services.AddScoped<ITaskService, TaskService>();
builder.Services.AddScoped<IUserService, UserService>();
builder.Services.AddScoped<IDALTask, DALTask>();
builder.Services.AddScoped<IDALUser, DALUser>();
builder.Services.AddScoped<IDALCategory, DALCategory>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
