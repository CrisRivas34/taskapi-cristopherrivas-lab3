using Xunit;
using Moq;
using taskapi_cristopherrivas_lab3.Controllers;
using taskapi_cristopherrivas_lab3.Services;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using taskapi_cristopherrivas_lab3;
using Microsoft.AspNetCore.Http.HttpResults;

public class TaskControllerTests
{
    private readonly Mock<ITaskService> _mockService;
    private readonly TaskController _controller;

    public TaskControllerTests()
    {
        _mockService = new Mock<ITaskService>();
        _controller = new TaskController(_mockService.Object);
    }

    [Fact]
    public void Get_ReturnsOkResult_WhenTasksExist()
    {
        _mockService.Setup(service => service.Get()).Returns(new List<MyTask>() as IEnumerable<MyTask>);

        var result = _controller.Get();

        Assert.IsType<OkObjectResult>(result);
    }

    [Fact]
    public void Post_ReturnsOkResult_WhenTaskIsSaved()
    {
        var newTask = new MyTask();
        _mockService.Setup(service => service.Save(newTask)).Verifiable();

        var result = _controller.Post(newTask);

        var okResult = Assert.IsType<CreatedResult>(result);
        _mockService.Verify();
    }

    [Fact]
    public void Put_ReturnsOkResult_WhenTaskIsUpdated()
    {
        var updatedTask = new MyTask();
        int taskId = 1;
        _mockService.Setup(service => service.Update(taskId, updatedTask)).Verifiable();

        var result = _controller.Put(taskId, updatedTask);

        var okResult = Assert.IsType<NoContentResult>(result);
        _mockService.Verify();
    }

    [Fact]
    public void Delete_ReturnsOkResult_WhenTaskIsDeleted()
    {
        int taskId = 1;
        _mockService.Setup(service => service.Delete(taskId)).Verifiable();

        var result = _controller.Delete(taskId);

        var okResult = Assert.IsType<NoContentResult>(result);
        _mockService.Verify();
    }

    [Fact]
    public void GetTaskStateHistory_ReturnsOkResult_WhenHistoryExists()
    {
        int taskId = 1;
        _mockService.Setup(service => service.GetTaskStateHistory(taskId)).Returns(new List<TaskStateHistory>());

        var result = _controller.GetTaskStateHistory(taskId);

        Assert.IsType<OkObjectResult>(result);
    }

    [Fact]
    public void AddTaskStateHistory_ReturnsOkResult_WhenHistoryIsAdded()
    {
        int taskId = 1;
        var newHistory = new TaskStateHistory();
        _mockService.Setup(service => service.AddTaskStateHistory(taskId, newHistory)).Verifiable();

        var result = _controller.AddTaskStateHistory(taskId, newHistory);

        var okResult = Assert.IsType<CreatedResult>(result);
        _mockService.Verify();
    }

}

public class UserControllerTests
{
    private readonly Mock<IUserService> _mockService;
    private readonly UserController _controller;

    public UserControllerTests()
    {
        _mockService = new Mock<IUserService>();
        _controller = new UserController(_mockService.Object);
    }

    [Fact]
    public void Get_ReturnsOkResult_WhenUsersExist()
    {
        _mockService.Setup(service => service.Get()).Returns(new List<User>());

        var result = _controller.Get();

        Assert.IsType<OkObjectResult>(result);
    }

    [Fact]
    public void Post_ReturnsCreatedResult_WhenUserIsSaved()
    {
        var newUser = new User();
        _mockService.Setup(service => service.Save(newUser)).Verifiable();

        var result = _controller.Post(newUser);

        Assert.IsType<CreatedResult>(result);
        _mockService.Verify();
    }

    [Fact]
    public void Put_ReturnsNoContentResult_WhenUserIsUpdated()
    {
        var updatedUser = new User();
        int userId = 1;
        _mockService.Setup(service => service.Update(userId, updatedUser)).Verifiable();

        var result = _controller.Put(userId, updatedUser);

        Assert.IsType<NoContentResult>(result);
        _mockService.Verify();
    }

    [Fact]
    public void Delete_ReturnsNoContentResult_WhenUserIsDeleted()
    {
        int userId = 1;
        _mockService.Setup(service => service.Delete(userId)).Verifiable();

        var result = _controller.Delete(userId);

        Assert.IsType<NoContentResult>(result);
        _mockService.Verify();
    }
}

public class CategoryControllerTests
{
    private readonly Mock<ICategoryService> _mockService;
    private readonly CategoryController _controller;

    public CategoryControllerTests()
    {
        _mockService = new Mock<ICategoryService>();
        _controller = new CategoryController(_mockService.Object);
    }

    [Fact]
    public void Get_ReturnsOkResult_WhenCategoriesExist()
    {
        _mockService.Setup(service => service.Get()).Returns(new List<Category>());

        var result = _controller.Get();

        Assert.IsType<OkObjectResult>(result);
    }

    [Fact]
    public void Post_ReturnsOkResult_WhenCategoryIsSaved()
    {
        var newCategory = new Category();
        _mockService.Setup(service => service.Save(newCategory)).Verifiable();

        var result = _controller.Post(newCategory);

        Assert.IsType<CreatedResult>(result);
        _mockService.Verify();
    }

    [Fact]
    public void Put_ReturnsOkResult_WhenCategoryIsUpdated()
    {
        var updatedCategory = new Category();
        int categoryId = 1;
        _mockService.Setup(service => service.Update(categoryId, updatedCategory)).Verifiable();

        var result = _controller.Put(categoryId, updatedCategory);

        Assert.IsType<NoContentResult>(result);
        _mockService.Verify();
    }

    [Fact]
    public void Delete_ReturnsOkResult_WhenCategoryIsDeleted()
    {
        int categoryId = 1;
        _mockService.Setup(service => service.Delete(categoryId)).Verifiable();

        var result = _controller.Delete(categoryId);

        Assert.IsType<NoContentResult>(result);
        _mockService.Verify();
    }
}
