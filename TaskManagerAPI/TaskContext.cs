using Microsoft.EntityFrameworkCore;

namespace taskapi_cristopherrivas_lab3.Models;

public class TaskContext: DbContext
{
    public DbSet<Category> Categories {get; set;}
    public DbSet<MyTask> Tasks {get;set;}
    public DbSet<User> Users {get;set;}
    public DbSet<TaskStateHistory> TaskStateHistories {get;set;}

    public TaskContext(DbContextOptions<TaskContext> options) :base(options) { }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Category>(category=>
        {
            category.ToTable("Category");
            category.HasKey(p=> p.CategoryId);
            category.Property(p=> p.Name).IsRequired().HasMaxLength(160);
            category.Property(p=> p.Description).IsRequired(false);
        });
        
        modelBuilder.Entity<MyTask>(task=> {
            task.ToTable("Task");
            task.HasKey(p=> p.Id);
            task.Property(p=> p.Title).IsRequired().HasMaxLength(160);
            task.Property(p=> p.Description).IsRequired(false);
            task.Property(p=> p.CreationDate);
            task.Property(p=> p.DueDate).IsRequired(false);
            task.Property(p=> p.IsCompleted);
            task.Property(p=> p.CategoryId);
            task.Property(p=> p.UserId);

        });

        modelBuilder.Entity<User>(user=> {
            user.ToTable("User");
            user.HasKey(p=> p.Id);
            user.Property(p=> p.Name).IsRequired().HasMaxLength(60);
        });

        modelBuilder.Entity<TaskStateHistory>(taskstate=> {
            taskstate.ToTable("TaskStateHistory");
            taskstate.HasKey(p=> p.Id);
            taskstate.Property(p=> p.TaskId);
            taskstate.Property(p=> p.State);
            taskstate.Property(p=> p.Timestamp);
        });
    }
}
