using taskapi_cristopherrivas_lab3.Models;
namespace taskapi_cristopherrivas_lab3.Services;

public class UserService : IUserService
{
    IDALUser DAL;

    public UserService(IDALUser dal)
    {
        this.DAL = dal;
    }

    public IEnumerable<User> Get()
    {
        return this.DAL.Get();
    }

    public async Task Save(User user)
    {
        await this.DAL.Save(user);
    }

    public async Task Update(int id, User user)
    {
        await this.DAL.Update(id, user);
    }

    public async Task Delete(int id)
    {
        await this.DAL.Delete(id);
    }

}

public interface IUserService
{
    IEnumerable<User> Get();
    Task Save(User user);
    Task Update(int id, User user);
    Task Delete(int id);
}