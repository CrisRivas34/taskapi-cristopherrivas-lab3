using taskapi_cristopherrivas_lab3.Models;
namespace taskapi_cristopherrivas_lab3.Services;

public class CategoryService : ICategoryService
{
    IDALCategory DAL;

    public CategoryService(IDALCategory dal)
    {
        this.DAL = dal;
    }

    public IEnumerable<Category> Get()
    {
        return this.DAL.Get();
    }

    public async Task Save(Category category) 
    {
        await this.DAL.Save(category);
    }

    public async Task Update(int id, Category category) 
    {
        await this.DAL.Update(id, category);
    }

    public async Task Delete(int id) 
    {
        await this.DAL.Delete(id);
    }
}

public interface ICategoryService
{
    IEnumerable<Category> Get();
    Task Save(Category category);
    Task Update(int id, Category category);
    Task Delete(int id);
}