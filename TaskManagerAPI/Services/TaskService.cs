using FluentValidation.Results;

using taskapi_cristopherrivas_lab3.Controllers;
using taskapi_cristopherrivas_lab3.DataAccessLayer;
using taskapi_cristopherrivas_lab3.Models;
namespace taskapi_cristopherrivas_lab3.Services;

public class TaskService : ITaskService
{
    IDALTask DAL;

    public TaskService(IDALTask dal)
    {
        this.DAL = dal;
    }

    public IEnumerable<MyTask> Get()
    {
        return this.DAL.Get();
    }

    public async Task Save(MyTask task)
    {
        TaskValidator validator = new TaskValidator();
        ValidationResult validationResult = validator.Validate(task);
        if (!validationResult.IsValid) {
            throw new Exception("validation task failed");
        }
        await this.DAL.Save(task);
    }

    public async Task Update(int id, MyTask task)
    {
        await this.DAL.Update(id, task);
    }

    public async Task Delete(int id)
    {
        await this.DAL.Delete(id);
    }

    public IEnumerable<TaskStateHistory> GetTaskStateHistory(int taskId)
    {
        return this.DAL.GetTaskStateHistory(taskId);
    }

    public async Task AddTaskStateHistory(int taskId, TaskStateHistory history)
    {
        await this.DAL.AddTaskStateHistory(taskId, history);
    }

    public async Task UpdateState(int id, TaskStateHistory taskState)
    {
        await this.DAL.UpdateState(id, taskState);
    }

    public async Task DeleteState(int id)
    {
        await this.DAL.DeleteState(id);
    }
}

public interface ITaskService
{
    IEnumerable<MyTask> Get();
    Task Save(MyTask task);
    Task Update(int id, MyTask task);
    Task Delete(int id);
    IEnumerable<TaskStateHistory> GetTaskStateHistory(int taskId);
    Task AddTaskStateHistory(int taskId, TaskStateHistory history);
    Task UpdateState(int id, TaskStateHistory taskState);
    Task DeleteState(int id);
}