using taskapi_cristopherrivas_lab3;

public interface IDALCategory
{
    IEnumerable<Category> Get();
    Task Save(Category category);
    Task Update(int id, Category category);
    Task Delete(int id);
}