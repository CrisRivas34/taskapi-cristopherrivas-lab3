using taskapi_cristopherrivas_lab3;

public interface IDALUser{
    IEnumerable<User> Get();
    Task Save(User user);
    Task Update(int id, User user);
    Task Delete(int id);
}