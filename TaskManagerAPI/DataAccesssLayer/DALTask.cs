using taskapi_cristopherrivas_lab3.Models;
namespace taskapi_cristopherrivas_lab3.DataAccessLayer;

public class DALTask : IDALTask
{
    TaskContext context;

    public DALTask(TaskContext dbcontext)
    {
        context = dbcontext;
    }

    public IEnumerable<MyTask> Get()
    {   
        return context.Tasks;
    }

    public async Task Delete(int id)
    {
        var taskToDelete = await context.Tasks.FindAsync(id);
        if (taskToDelete != null)
        {
            context.Tasks.Remove(taskToDelete);
            await context.SaveChangesAsync();
        }
    }

    public async Task Save(MyTask task)
    {
        context.Tasks.Add(task);
        await context.SaveChangesAsync();
    }

    public async Task Update(int id, MyTask task)
    {
        var existingTask = await context.Tasks.FindAsync(id);
        if (existingTask != null)
        {
            existingTask.Title = task.Title;
            existingTask.Description = task.Description;
            existingTask.PriorityTask = task.PriorityTask;
            existingTask.CreationDate = task.CreationDate;
            await context.SaveChangesAsync();
        }
    }

    public async Task AddTaskStateHistory(int taskId, TaskStateHistory history)
    {
        history.TaskId = taskId;
        context.TaskStateHistories.Add(history);
        await context.SaveChangesAsync();
    }

        public IEnumerable<TaskStateHistory> GetTaskStateHistory(int taskId)
    {
        return context.TaskStateHistories.Where(h => h.TaskId == taskId);
    }

    public async Task UpdateState(int id, TaskStateHistory taskState)
    {
        var existingTask = await context.TaskStateHistories.FindAsync(id);
        if (existingTask != null)
        {
            existingTask.State = taskState.State;
            await context.SaveChangesAsync();
        }
    }

    public async Task DeleteState(int id)
    {
        var taskToDelete = await context.TaskStateHistories.FindAsync(id);
        if (taskToDelete != null)
        {
            context.TaskStateHistories.Remove(taskToDelete);
            await context.SaveChangesAsync();
        }
    }
}
