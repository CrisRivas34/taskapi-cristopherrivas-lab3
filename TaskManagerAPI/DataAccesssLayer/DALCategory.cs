using taskapi_cristopherrivas_lab3.Models;
namespace taskapi_cristopherrivas_lab3.DataAccessLayer;

public class DALCategory : IDALCategory
{
    TaskContext context;

    public DALCategory(TaskContext dbcontext)
    {
        context = dbcontext;
    }

    public async Task Delete(int id)
    {
        var categoryToDelete = await context.Categories.FindAsync(id);
        if (categoryToDelete != null)
        {
            context.Categories.Remove(categoryToDelete);
            await context.SaveChangesAsync();
        }
    }

    public IEnumerable<Category> Get()
    {
        return context.Categories;
    }

    public async Task Save(Category category)
    {
        context.Categories.Add(category);
        await context.SaveChangesAsync();
    }

    public async Task Update(int id, Category category)
    {
        var existingCategory = await context.Categories.FindAsync(id);
        if (existingCategory != null)
        {
            existingCategory.Name = category.Name;
            existingCategory.Description = category.Description;
            await context.SaveChangesAsync();
        }
    }
}
