using taskapi_cristopherrivas_lab3.Models;
namespace taskapi_cristopherrivas_lab3.DataAccessLayer;

public class DALUser : IDALUser
{
    TaskContext context;

    public DALUser(TaskContext dbcontext)
    {
        context = dbcontext;
    }
    
    public IEnumerable<User> Get()
    {
        return context.Users;
    }

    public async Task Delete(int id)
    {
        var userToDelete = await context.Users.FindAsync(id);
        if (userToDelete != null)
        {
            context.Users.Remove(userToDelete);
            await context.SaveChangesAsync();
        }
    }

    public async Task Save(User user)
    {
        context.Users.Add(user);
        await context.SaveChangesAsync();
    }

    public async Task Update(int id, User user)
    {
        var existingUser = await context.Users.FindAsync(id);
        if (existingUser != null)
        {
            existingUser.Name = user.Name;
            await context.SaveChangesAsync();
        }
    }
}