using taskapi_cristopherrivas_lab3;

public interface IDALTask{
    IEnumerable<MyTask> Get();
    Task Save(MyTask task);
    Task Update(int id, MyTask task);
    Task Delete(int id);
    IEnumerable<TaskStateHistory> GetTaskStateHistory(int taskId);
    Task AddTaskStateHistory(int taskId, TaskStateHistory history);
    Task UpdateState(int id, TaskStateHistory taskState);
    Task DeleteState(int id);
}
