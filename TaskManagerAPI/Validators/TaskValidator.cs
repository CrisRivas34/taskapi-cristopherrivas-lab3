using FluentValidation;

namespace taskapi_cristopherrivas_lab3.Controllers;

public class TaskValidator : AbstractValidator<MyTask>
{
    public TaskValidator()
    {
        RuleFor(task => task.Title).NotEmpty().WithMessage("The Tittle is required");
        RuleFor(task => task.DueDate).GreaterThanOrEqualTo(task => task.CreationDate)
            .WithMessage("End date cannot be earlier than creation date");
    }
}
