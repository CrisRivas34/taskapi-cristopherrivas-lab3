# Task Manager API Laboratorio 5

Task Manager API is an ASP.NET Core application that provides a set of endpoints to manage tasks.

## Documentation of the migration is on the wiki

## Execution

To run the application, follow these steps:

1. Make sure you have .NET Core SDK and docker installed on your machine.
2. Clone this repository to your local machine.
3. Open a terminal in the root directory of the application.
4. Run the command `sudo docker compose up -d` to start the MySQL database server and the application.
5. Run the command `dotnet run` to start the server.
6. Once the server is up and running, you can access the API via the URL: `http://localhost:5228`.

## Tests Execution

Use `dotnet test` to execute all tests.

## Swagger Documentation

This API is documented using Swagger. You can explore the API documentation by navigating to `/swagger/index.html` after running the application.
