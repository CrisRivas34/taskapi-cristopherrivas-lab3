# Task Manager API Laboratorio 10

Task Manager API is an ASP.NET Core application that provides a set of endpoints to manage tasks.

## Documentation of the migration is on the wiki

## Execution

To run the application, follow these steps:

1. Make sure you have .NET Core SDK and docker installed on your machine.
2. Clone this repository to your local machine.
3. Open a terminal in the root directory of the application.
4. Run the command `sudo docker compose up -d` to start the MySQL database server and the application.
5. Run the command `dotnet run` to start the server.
6. Once the server is up and running, you can access the API via the URL: `http://localhost:5228`.

## Tests Execution

Use `dotnet test` to execute all tests.

## Swagger Documentation

This API is documented using Swagger. You can explore the API documentation by navigating to `/swagger/index.html` after running the application.

# CLI 
Task Manager CLI is a command-line interface application built to interact with the Task Manager API.

## Installation
To install the Task Manager CLI, follow these steps:

1. Clone the Task Manager CLI repository to your local machine.
2. Navigate to the root directory of the CLI application.
3. Open a terminal in the root directory.
4. Run the command dotnet build to build the application.
5. Once the build is complete, you can run the CLI using the command dotnet run.

## Usage
The Task Manager CLI provides the functions:
1. Get all users: Retrieves all users from the Task Manager API.
2. Create user: Creates a new user with the specified ID and name.
3. Update user: Updates the name of the user with the specified ID.
4. Delete user: Deletes the user with the specified ID.
5. Get all tasks: Retrieves all tasks from the Task Manager API.
6. Create task: Creates a new task with the specified ID, title, description, completion status, category ID, and user ID.
7. Delete task: Deletes the task with the specified ID.
8. Get all categories: Retrieves all categories from the Task Manager API.
9. Create category: Creates a new category with the specified ID, name, and description.
10. Delete category: Deletes the category with the specified ID.
0. Exit: Exits the Task Manager CLI.

# Library
## CategoryService Documentation:

The CategoryService class provides methods to interact with the Category resource in the Task Manager API.

Methods:
  - GetAllCategories(): Retrieves all categories from the Task Manager API.
    - Returns: A collection of Category objects representing the retrieved categories.

  - CreateCategory(Category category): Creates a new category.
    - Parameters:
      - category: The Category object to be created.
    - Throws:
      - ArgumentNullException: If the provided category is null.
      - Exception: If an error occurs during the creation process.

  - UpdateCategory(int categoryId, Category category): Updates an existing category.
    - Parameters:
      - categoryId: The ID of the category to be updated.
      - category: The updated Category object.
    - Throws:
      - Exception: If an error occurs during the update process.

  - DeleteCategory(int categoryId): Deletes a category by its ID.
    - Parameters:
      - categoryId: The ID of the category to be deleted.
    - Throws:
      - Exception: If an error occurs during the deletion process.

## UserService Documentation:

The UserService class provides methods to interact with the User resource in the Task Manager API.

Methods:
  - GetAllUsers(): Retrieves all users from the Task Manager API.
    - Returns: A collection of User objects representing the retrieved users.

  - CreateUser(User user): Creates a new user.
    - Parameters:
      - user: The User object to be created.
    - Throws:
      - ArgumentNullException: If the provided user is null.
      - Exception: If an error occurs during the creation process.

  - UpdateUser(int userId, User user): Updates an existing user.
    - Parameters:
      - userId: The ID of the user to be updated.
      - user: The updated User object.
    - Throws:
      - Exception: If an error occurs during the update process.

  - DeleteUser(int userId): Deletes a user by its ID.
    - Parameters:
      - userId: The ID of the user to be deleted.
    - Throws:
      - Exception: If an error occurs during the deletion process.

## MyTaskService Documentation:

The MyTaskService class provides methods to interact with the MyTask resource in the Task Manager API.

Methods:
  - GetAllTasks(): Retrieves all tasks from the Task Manager API.
    - Returns: A collection of MyTask objects representing the retrieved tasks.

  - CreateTask(MyTask myTask): Creates a new task.
    - Parameters:
      - myTask: The MyTask object to be created.
    - Throws:
      - ArgumentNullException: If the provided myTask is null.
      - Exception: If an error occurs during the creation process.

  - UpdateTask(int myTaskId, MyTask myTask): Updates an existing task.
    - Parameters:
      - myTaskId: The ID of the task to be updated.
      - myTask: The updated MyTask object.
    - Throws:
      - Exception: If an error occurs during the update process.

  - DeleteTask(int myTaskId): Deletes a task by its ID.
    - Parameters:
      - myTaskId: The ID of the task to be deleted.
    - Throws:
      - Exception: If an error occurs during the deletion process.

## TaskStateHistoryService Documentation:

The TaskStateHistoryService class provides methods to interact with the Task State History resource in the Task Manager API.

Methods:
  - GetTaskState(int taskStateId): Retrieves the state history of a specific task.
    - Parameters:
      - taskStateId: The ID of the task whose state history is to be retrieved.
    - Returns: A collection of TaskStateHistory objects representing the state history of the specified task.

  - CreateTaskState(TaskStateHistory taskState, int taskStateId): Creates a new task state history entry for a specific task.
    - Parameters:
      - taskState: The TaskStateHistory object representing the new state history entry.
      - taskStateId: The ID of the task for which the state history entry is created.
    - Throws:
      - ArgumentNullException: If the provided taskState is null.
      - Exception: If an error occurs during the creation process.

  - UpdateTaskState(int taskStateId, TaskStateHistory taskState): Updates an existing task state history entry.
    - Parameters:
      - taskStateId: The ID of the task whose state history entry is to be updated.
      - taskState: The updated TaskStateHistory object.
    - Throws:
      - Exception: If an error occurs during the update process.

  - DeleteUser(int taskStateId): Deletes all state history entries of a task by its ID.
    - Parameters:
      - taskStateId: The ID of the task whose state history entries are to be deleted.
    - Throws:
      - Exception: If an error occurs during the deletion process.

