using System.Net.Http.Headers;
using Microsoft.VisualBasic;
using TaskManagerLibrary.Models;
using TaskManagerLibrary.Services;

namespace TaskManagerCLI;

public class TaskManager
{
    HttpClient httpClient = new HttpClient();
    public UserService userService;
    public MyTaskService myTaskService;
    public CategoryService categoryService;

    public TaskManager()
    {
        httpClient.BaseAddress = new Uri("http://localhost:5228");
        httpClient.DefaultRequestHeaders.Accept.Clear();
        httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        userService = new UserService(httpClient);
        myTaskService = new MyTaskService(httpClient);
        categoryService = new CategoryService(httpClient);
        Console.WriteLine("Conection Complete");
    }
    
    public async Task GetAllUsersAsync()
    {
        try
        {
            Console.WriteLine("Getting users...");
            var users = await userService.GetAllUsers();
            Console.WriteLine("Showing users:");
            foreach (var user in users)
            {
                Console.WriteLine($"ID: {user.Id}, Name: {user.Name}");
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error: {ex.Message}");
        }
    }

    public async Task CreateUserAsync(int id, string name)
    {
        try
        {
            User newUser = new User { Id = id, Name = name, Tasks = new List<MyTask>()};
            await userService.CreateUser(newUser);
            Console.WriteLine($"New User {id} {name} was created.");
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error: {ex.Message}");
        }
    }

    public async Task UpdateUserAsync(int id, string name)
    {
        try
        {
            User newUser = new User { Id = id, Name = name, Tasks = new List<MyTask>()};
            await userService.UpdateUser(id, newUser);
            Console.WriteLine($"User {id} {name} was edited.");
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error: {ex.Message}");
        }
    }

    public async Task DeleteUserAsync(int id)
    {
        try
        {
            await userService.DeleteUser(id);
            Console.WriteLine($"User {id} was deleted.");
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error: {ex.Message}");
        }
    }

    public async Task GetAllTasksAsync()
    {
        try
        {
            Console.WriteLine("Getting tasks...");
            var tasks = await myTaskService.GetAllTasks();
            Console.WriteLine("Showing tasks:");
            foreach (var task in tasks)
            {
                Console.WriteLine($"ID: {task.Id}, Title: {task.Title} Description: {task.Description}, Creation date: {task.CreationDate}, Due date: {task.DueDate}, ProrityTask: {task.PriorityTask}, Is Completed: {task.IsCompleted}");
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error: {ex.Message}");
        }
    }

    public async Task CreateTaskAsync(int id, string title, string description, Boolean IsCompleted, int categoryID, int userID)
    {
        try
        {
            MyTask newTask = new MyTask { Id = id, Title = title, Description = description, CreationDate = DateTime.Today, PriorityTask = Priority.No_Priority, 
                IsCompleted = false, CategoryId = categoryID, UserId = userID};
            await myTaskService.CreateTask(newTask);
            Console.WriteLine($"New Task {id} {title} was created.");
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error: {ex.Message}");
        }
    }

    public async Task DeleteTaskAsync(int id)
    {
        try
        {
            await myTaskService.DeleteTask(id);
            Console.WriteLine($"Task {id} was deleted.");
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error: {ex.Message}");
        }
    }

    public async Task GetAllCategoriesAsync()
    {
        try
        {
            Console.WriteLine("Getting categories...");
            var categories = await categoryService.GetAllCategories();
            Console.WriteLine("Showing categories:");
            foreach (var category in categories)
            {
                Console.WriteLine($"ID: {category.CategoryId}, Name: {category.Name}, Description: {category.Description}");
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error: {ex.Message}");
        }
    }

    public async Task CreateCategoryAsync(int id, string name, string description)
    {
        try
        {
            Category category = new Category { CategoryId = id, Name = name, Description = description};
            await categoryService.CreateCategory(category);
            Console.WriteLine($"New category {id} {name} was created.");
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error: {ex.Message}");
        }
    }

    public async Task DeleteCategoryAsync(int id)
    {
        try
        {
            await categoryService.DeleteCategory(id);
            Console.WriteLine($"Category {id} was deleted.");
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error: {ex.Message}");
        }
    }
}
