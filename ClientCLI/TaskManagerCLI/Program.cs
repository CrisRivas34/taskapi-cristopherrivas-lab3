﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using TaskManagerLibrary.Models;
using TaskManagerLibrary.Services;

namespace TaskManagerCLI
{
    class Program
    {
        static async Task Main(string[] args)
        {
            TaskManager taskManager = new TaskManager();
            Console.WriteLine("Welcome to Task Manager CLI!");

            while (true)
            {
                Console.WriteLine("\nSelect an option:");
                Console.WriteLine("1. Get all users");
                Console.WriteLine("2. Create user");
                Console.WriteLine("3. Update user");
                Console.WriteLine("4. Delete user");
                Console.WriteLine("5. Get all tasks");
                Console.WriteLine("6. Create task");
                Console.WriteLine("7. Delete task");
                Console.WriteLine("8. Get all categories");
                Console.WriteLine("9. Create category");
                Console.WriteLine("10. Delete category");
                Console.WriteLine("0. Exit");

                int option;
                if (!int.TryParse(Console.ReadLine(), out option))
                {
                    Console.WriteLine("Invalid option. Please enter a number.");
                    continue;
                }

                switch (option)
                {
                    case 0:
                        Console.WriteLine("Exiting...");
                        return;
                    case 1:
                        await taskManager.GetAllUsersAsync();
                        break;
                    case 2:
                        Console.WriteLine("Enter user ID:");
                        int userId = int.Parse(Console.ReadLine());
                        Console.WriteLine("Enter user name:");
                        string userName = Console.ReadLine();
                        await taskManager.CreateUserAsync(userId, userName);
                        break;
                    case 3:
                        Console.WriteLine("Enter user ID to update:");
                        int userIdToUpdate = int.Parse(Console.ReadLine());
                        Console.WriteLine("Enter new user name:");
                        string newUserName = Console.ReadLine();
                        await taskManager.UpdateUserAsync(userIdToUpdate, newUserName);
                        break;
                    case 4:
                        Console.WriteLine("Enter user ID to delete:");
                        int userIdToDelete = int.Parse(Console.ReadLine());
                        await taskManager.DeleteUserAsync(userIdToDelete);
                        break;
                    case 5:
                        await taskManager.GetAllTasksAsync();
                        break;
                    case 6:
                        Console.WriteLine("Enter task ID:");
                        int taskId = int.Parse(Console.ReadLine());
                        Console.WriteLine("Enter task title:");
                        string taskTitle = Console.ReadLine();
                        Console.WriteLine("Enter task description:");
                        string taskDescription = Console.ReadLine();
                        Console.WriteLine("Enter task completion status (true/false):");
                        bool isCompleted = bool.Parse(Console.ReadLine());
                        Console.WriteLine("Enter category ID:");
                        int categoryId = int.Parse(Console.ReadLine());
                        Console.WriteLine("Enter user ID:");
                        int userIdForTask = int.Parse(Console.ReadLine());
                        await taskManager.CreateTaskAsync(taskId, taskTitle, taskDescription, isCompleted, categoryId, userIdForTask);
                        break;
                    case 7:
                        Console.WriteLine("Enter task ID to delete:");
                        int taskIdToDelete = int.Parse(Console.ReadLine());
                        await taskManager.DeleteTaskAsync(taskIdToDelete);
                        break;
                    case 8:
                        await taskManager.GetAllCategoriesAsync();
                        break;
                    case 9:
                        Console.WriteLine("Enter category ID:");
                        categoryId = int.Parse(Console.ReadLine());
                        Console.WriteLine("Enter category name:");
                        string categoryName = Console.ReadLine();
                        Console.WriteLine("Enter category description:");
                        string categoryDescription = Console.ReadLine();
                        await taskManager.CreateCategoryAsync(categoryId, categoryName, categoryDescription);
                        break;
                    case 10:
                        Console.WriteLine("Enter category ID to delete:");
                        int categoryIdToDelete = int.Parse(Console.ReadLine());
                        await taskManager.DeleteCategoryAsync(categoryIdToDelete);
                        break;
                    default:
                        Console.WriteLine("Invalid option. Please select a valid option.");
                        break;
                }
            }
        }        
    }
}
